#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include <vector>

#define LOG(message) std::cerr << "[DEBUG] " << message << std::endl
#define TRACE(message)                                     \
    std::cerr << "[TRACE] " << __FILE__ << "/" << __LINE__ \
              << "/ " #message " : " << message << std::endl
#define COUT(message) std::cout << message << std::endl

std::vector<std::string> split(const std::string& s, char delim) {
    std::vector<std::string> elems;
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        if (!item.empty()) {
            elems.push_back(item);
        }
    }
    return elems;
}

int main(int argc, char** argv) {
    if (argc < 3) {
        COUT("argument error");
        exit(1);
    }

    std::string text_path = argv[1];
    std::string out_text_path = argv[2];
    COUT(text_path);
    COUT(out_text_path);

    std::ifstream file(text_path);
    if (!file.is_open()) {
        throw std::runtime_error("Couldn't read " + text_path + ".");
    }

    std::string line;
    std::set<std::string> string_set;
    // while (file >> line) {
    while (std::getline(file, line)) {
        // COUT(line);
        // string_set.insert(line);
        std::vector<std::string> hoge = split(line, '\t');
        // COUT((int)hoge[2][0] << " : " << hoge[2][0]);
        string_set.insert(hoge[2]);
        // COUT(hoge.size());
        // return 0;
    }
    // return 0;

    std::ofstream ofile(out_text_path);
    for (auto iter = string_set.begin(); iter != string_set.end(); ++iter) {
        // COUT(*iter);
        if (iter->empty()) {
            continue;
        }
        ofile << *iter << std::endl;
    }
}
